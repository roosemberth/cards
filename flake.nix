{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = inputs: inputs.flake-utils.lib.eachDefaultSystem (system:
    with builtins;
    let
      pkgs = import inputs.nixpkgs { inherit system; };
      haskellDeps = drv: concatLists (attrValues drv.getCabalDeps);
      hs = pkgs.haskellPackages;

      cards = hs.callCabal2nix "cards" ./. {};
    in
    {
      packages = {
        inherit cards;
        default = cards;
      };

      devShells.default = pkgs.mkShell {
        nativeBuildInputs = [
          (hs.ghcWithPackages (ps: haskellDeps cards))
          hs.cabal-install
          hs.haskell-language-server
          hs.hpack
          pkgs.clang
        ];
      };
    }
  );
}

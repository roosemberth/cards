module Pages where

import Components
import Components.FormInput
import Components.Modal
import Control.Monad (void)
import Data.Map (Map)
import Data.Text
import Reflex.Dom

pageHead :: Widget w ()
pageHead = do
  elAttr
    "meta"
    ("name" =: "viewport" <> "content" =: "width=device-width, initial-scale=1")
    blank
  elAttr
    "link"
    ("rel" =: "stylesheet" <> "href" =: "resources/bulma.min.css")
    blank
  elAttr
    "link"
    ("rel" =: "stylesheet" <> "href" =: "resources/font-awesome.min.css")
    blank

showStartPage :: IO ()
showStartPage = mainWidgetWithHead pageHead (pageLayout @StartPage)

data StartPage

instance PageLayout StartPage where
  pageBody = mdo
    addCardEv <- divClass "container" $ do
      cardsSection newCardEv
    newCardEv <- addCardModal addCardEv
    pure ()

-- | Returns a Modal returning the new card provided by the user.
addCardModal :: forall t m. MonadWidget t m => Event t () -> m (Event t Card)
addCardModal evOpen = toWidget $ mkModalComponent @t @m evOpen $
  elAttr "form" ("class" =: "box" <> "onsubmit" =: "return false") $ do
    front <- toWidget $ (defFormField @t @m textInput')
      { label = Just "Front of the card"
      , placeholder = Just "e.g. the answer to the universe"
      }
    back <- toWidget $ (defFormField @t @m textInput')
      { label = Just "Back of the card"
      , placeholder = Just "e.g. 42"
      }
    submitEv <- fmap (domEvent Click) $ divClass "field" $ do
      divClass "field" $
        fst <$> elAttr' "button"
          ("class" =: "button is-success")
          (text "Add new card")
    let cardDyn = Card <$> front <*> back
    pure $ tagPromptlyDyn cardDyn submitEv

-- | Returns an event representing a user's intent to add a card.
cardsSection :: MonadWidget t m => Event t Card -> m (Event t ())
cardsSection newCardEv =
  divClass "columns is-multiline" $ mdo
    cards <- foldDyn (:) sampleCards newCardEv
    dyn $ mapM_ toWidget <$> cards
    -- "Add new" card
    (el, _) <- toWidget' (IconCard "fa-solid fa-plus")
    pure (void (domEvent Click el))

data Card = Card { front :: Text, back :: Text }

sampleCards :: [Card]
sampleCards =
  [ Card "abholen" "to pick up"
  , Card "abnehmen" "to take off"
  , Card "anfangen" "to start, begin"
  , Card "anrufen" "to call, telephone"
  , Card "anziehen" "to put on, dress"
  ]

toCardLayout :: MonadWidget t m => m () -> m (El t, ())
toCardLayout w =
  elAttr' "div" ("class" =: "column is-one-third") $
    divClass "box" $
      divClass "container" w

instance MonadWidget t m => ElemLayout Card t m () where
  toWidget' c = toCardLayout $ text (front c)

newtype IconCard = IconCard Text

instance MonadWidget t m => ElemLayout IconCard t m () where
  toWidget' (IconCard cls) =
    toCardLayout $
      elClass "span" "icon is-small" $
        elClass "p" cls blank

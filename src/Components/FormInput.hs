module Components.FormInput where

import Components
import Control.Monad (guard)
import Data.Default
import Data.Foldable (fold)
import Data.Map (Map)
import Data.Map.Lazy (mapKeys)
import Data.Maybe (catMaybes)
import Data.Text
import Reflex.Dom

data FormFieldData t m r = FormFieldData
  { el :: Text
  , label :: Maybe Text
  , placeholder :: Maybe Text
  , required :: Bool
  , mkField :: MonadWidget t m => Map Text Text -> m r
  }

defFormField :: MonadWidget t m => (Map Text Text -> m r) -> FormFieldData t m r
defFormField mk = FormFieldData
    { el = "div"
    , label = Nothing
    , placeholder = Nothing
    , required = True
    , mkField = mk
    }

instance MonadWidget t m => Default (FormFieldData t m ()) where
  def = defFormField (\attrs -> elAttr "input" attrs blank)

instance MonadWidget t m => ElemLayout (FormFieldData t m r) t m r where
  toWidget' FormFieldData{..} =
    elAttr' "div" ("class" =: "field") $ do
      mIfJust label (elClass "label" "label" . text)
      divClass "control" $
        mkField $
             "class" =: "control"
          <> "type" =: el
          <> fold (catMaybes
            [ ("placeholder" =:) <$> placeholder
            , ("required" =:) <$> toMaybe "" required
            ])
    where
      mIfJust m f = maybe (pure ()) f m
      toMaybe v p = v <$ guard p

textInput' :: MonadWidget t m => Map Text Text -> m (Dynamic t Text)
textInput' attrs = fmap _inputElement_value <$> inputElement $ def &
  inputElementConfig_elementConfig
    . elementConfig_initialAttributes
    .~ mapKeys (AttributeName Nothing) attrs

module Components.Modal where

import Components
import Control.Monad (void)
import Reflex.Dom

data ModalComponent t m r = ModalComponent
  { background :: MonadWidget t m => m ()
  , content :: MonadWidget t m => m (Event t r)
  , openEv :: Event t ()
  }

mkModalComponent :: MonadWidget t m =>
  Event t () -> m (Event t r) -> ModalComponent t m r
mkModalComponent openEv c = ModalComponent
  { background = blank
  , content = c
  , openEv
  }

instance MonadWidget t m =>
  ElemLayout (ModalComponent t m r) t m (Event t r) where
  toWidget' ModalComponent{..} = mdo
    let showEv = "modal is-active" <$ openEv
        hideEv = "modal" <$ leftmost [void rEv, closeEv]
    classDyn <- holdDyn "modal" $ leftmost [hideEv, showEv]
    (el, (rEv, closeEv)) <- elDynAttr' "div" (("class" =:) <$> classDyn) $ do
      closeEv <- domEvent Click . fst <$>
        elAttr' "div" ("class" =: "modal-background") background
      rEv <- divClass "modal-content" content
      pure (rEv, closeEv)
    pure (el, rEv)

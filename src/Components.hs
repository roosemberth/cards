{-# LANGUAGE FunctionalDependencies #-}
module Components where

import Reflex.Dom

-- | A page containing some layout.
class PageLayout p where
  pageLayout :: MonadWidget t m => m ()
  pageLayout =
    elAttr "section" ("class" =: "hero is-primary is-fullheight") $
      divClass "hero-body" (pageBody @p)
  pageBody :: MonadWidget t m => m ()

-- | Some data with a widget representation.
class MonadWidget t m => ElemLayout p t m r | p t m -> r where
  toWidget' :: p -> m (El t, r)
  toWidget :: p -> m r
  toWidget = fmap snd . toWidget'
